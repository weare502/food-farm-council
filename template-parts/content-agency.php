<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Food_Farm_Council
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				food_farm_council_posted_on();
				food_farm_council_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	
	<section id="agencycontent">
		<section id="sidebar" class="item">
			<?php if( get_field('logo_image') ): ?><div class="logo_image">
				<img src="<?php the_field('logo_image');?>">
			</div><?php endif; ?>
			<div class="sidecontent">
				<h4>Contact Information</h4>
				<?php if( get_field('address') ): ?><div class="address">
					<h5>Address</h5>
					<ul><li><?php the_field('address');?></li></ul>
				</div><?php endif; ?>
				<?php if( get_field('website_link') || get_field('website_text') ): ?><div class="website">
					<h5>Website</h5>
					<ul><li><a target="_blank" href="<?php the_field('website_link');?>"><?php the_field('website_text');?></a></li></ul>
				</div><?php endif; ?>
				<?php if( get_field('key_contact_name') || get_field('key_contact_name') ): ?><div class="keycontact">
					<h5>Key Contact Name</h5>
					<ul><li><?php the_field('key_contact_name');?></li></ul>
				</div><?php endif; ?>
				<?php if( get_field('email') ): ?><div class="email">
					<h5>Email</h5>
					<ul><li><a href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a></li></ul>
				</div><?php endif; ?>
				<?php if( get_field('phone') ): ?><div class="phone">
					<h5>Phone</h5>
					<ul><li><a href="tel://<?php the_field('phone');?>"><?php the_field('phone');?></a></li></ul>
				</div><?php endif; ?>
				<?php if( get_field('fax') ): ?><div class="fax">
					<h5>Fax</h5>
					<ul><li><a href="tel://<?php the_field('fax');?>"><?php the_field('fax');?></a></li></ul>
				</div><?php endif; ?>
				<div class="social">
					<h5>Social Links</h5>
					<?php if( have_rows('social_links') ):?>
					<ul id="agsocial">
					
					    <?php while( have_rows('social_links') ) : the_row(); ?>
					    
						<?php if( get_sub_field('facebook') ): ?><li><a href="<?php the_sub_field('facebook');?>"><i class="fab fa-facebook"></i></a></li><?php endif; ?>
						<?php if( get_sub_field('twitter') ): ?><li><a href="<?php the_sub_field('twitter');?>"><i class="fab fa-twitter"></i></a></li><?php endif; ?>
						<?php if( get_sub_field('instagram') ): ?><li><a href="<?php the_sub_field('instagram');?>"><i class="fab fa-instagram"></i></a></li><?php endif; ?>
						<?php if( get_sub_field('linkedin') ): ?><li><a href="<?php the_sub_field('linkedin');?>"><i class="fab fa-linkedin"></i></a></li><?php endif; ?>

							<?php
					        // Do something...
					        
					
					    // End loop.
					    endwhile;?>
					</ul>
					
					<?php // No value.
					else :
					    // Do something...
					endif;
					?>
				</div>
			</div>
		</section>
		<section id="agencybody" class="item">
			<?php if( get_field('overview_text') ): ?><div class="item mission">
				<!--<div class="item quotes">"</div>-->
				<div class="item"><?php the_field('overview_text');?></div>
			</div><?php endif; ?>
			<?php if( get_field('image_1') || get_field('image_2') ){ ?><div class="item images">
			<?php if( get_field('image_1')){ ?>
				<div class="image1" style="background: url('<?php the_field('image_1');?>') center center; background-size: cover;"><div class="orangeborder"></div></div>
				<?php } 
				 if( get_field('image_2')) { ?>
				<div class="image2" style="background: url('<?php the_field('image_2');?>') center center; background-size: cover;"><div class="orangeborder"></div></div>
				<?php } ?>
			</div><?php } ?>
			<div class="item agcontent">
				<div class="entry-content">
					<?php
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'food-farm-council' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							wp_kses_post( get_the_title() )
						)
					);
			
					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'food-farm-council' ),
							'after'  => '</div>',
						)
					);
					?>
				</div><!-- .entry-content -->

			</div>
		</section>
	</section>


	<footer class="entry-footer">
		<?php food_farm_council_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
