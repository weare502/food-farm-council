<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Food_Farm_Council
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">
			
			<div class="page-content">
				<?php the_field('error_content','options');?>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
