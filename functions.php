<?php
/**
 * Food Farm Council functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Food_Farm_Council
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'food_farm_council_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function food_farm_council_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Food Farm Council, use a find and replace
		 * to change 'food-farm-council' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'food-farm-council', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'food-farm-council' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'food_farm_council_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'food_farm_council_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function food_farm_council_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'food_farm_council_content_width', 640 );
}
add_action( 'after_setup_theme', 'food_farm_council_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function food_farm_council_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'food-farm-council' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'food-farm-council' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'food_farm_council_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function food_farm_council_scripts() {
	wp_enqueue_style( 'food-farm-council-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'matt-style' , get_stylesheet_directory_uri() . '/matt.css' );
	wp_enqueue_style( 'ffc-style-overrides', get_stylesheet_directory_uri() . '/ffc-styles.css', [], '10112021' ); // SCSS styles
	wp_style_add_data( 'food-farm-council-style', 'rtl', 'replace' );

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js');
	wp_enqueue_script( 'food-farm-council-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'food_farm_council_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* CCG FUNCTIONS */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options Settings',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function ccg_register_cpts_agency() {
register_post_type( 'agency ',
        array(
            'labels' => array(
                'name' => __( 'Agencies' ),
                'singular_name' => __( 'Agency' )
            ),
            'public' => true,
            'has_archive' => true,
			//'menu_icon' => 'dashicons-*',
			'show_in_rest' => true,
			)
    );

}
add_action( 'init', 'ccg_register_cpts_agency' );

add_action( 'init', 'ccg_create_taxonomies' );

function ccg_create_taxonomies() {
	register_taxonomy(
		'agency-category',
		'agency',
		array(
			'label' => 'Agency Categories',
			'show_admin_column' => true,
			'show_in_rest' => true,
			'hierarchical' => true,
		)
	);
	
}


function cs_custom_posts_per_page( $query ) {

	if ( is_tax( 'agency-category' ) ) {
        $query->query_vars['posts_per_page'] = -1;
        return;
    }
}
add_filter( 'pre_get_posts', 'cs_custom_posts_per_page' );


function acf_load_category_check_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();
	$value = 'value';
	$label = 'label';
	$field['choices'][ $value ] = $label;
	return $field;
	
}
//add_filter('acf/load_field/name=category_check', 'acf_load_category_check_field_choices');



add_action('wp_ajax_lookbookfilter', 'misha_filter_function'); 
add_action('wp_ajax_nopriv_lookbookfilter', 'misha_filter_function');


function misha_filter_function(){
$pagefilter = 1;
if( isset( $_POST['pagefilter'] ) ) {
		if (is_numeric($_POST['pagefilter'])){
			$pagefilter = $_POST['pagefilter'];
		}
	}
	
$args = array(
	'posts_per_page'	=> 15 * $pagefilter,
	'post_type'		=> 'post',
	'post_status' => 'publish',
	//'order' => 'DESC',
	//'orderby'	=> 'post_date',
	//'suppress_filters' => true
	);
	
if($_POST['categoryfilter']) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $_POST['categoryfilter']
			)
		);
	}
	
$the_query = new WP_Query( $args );
if( $the_query->have_posts() ){
	$total_results = $the_query->found_posts;
	$pagesnum = $the_query->max_num_pages;
	//echo $pagesnum;
	echo '<script>$( ".indexcontent" ).attr( "data-pagesnum", "' . $pagesnum . '" );</script>';
	while ( $the_query->have_posts() ) { $the_query->the_post();
		get_template_part( 'template-parts/content', 'index' );
	}
	
}
else {
	echo '<div>No Posts Found</div>';
}
wp_reset_postdata();
die();

}

add_action('wp_enqueue_scripts', 'enqueue_magnificpopup_scripts');
function enqueue_magnificpopup_scripts() {
    wp_register_script('magnific_popup_script', get_stylesheet_directory_uri().'/magnific-popup/jquery.magnific-popup.min.js', array('jquery'));
    wp_enqueue_script('magnific_popup_script');
    //wp_register_script('magnific_init_script', get_stylesheet_directory_uri().'/magnific-popup/jquery.magnific-popup-init.js', array('jquery'));
    //wp_enqueue_script('magnific_init_script');
	wp_enqueue_style( 'magnific_popup_style', get_stylesheet_directory_uri() . '/magnific-popup/magnific-popup.css', false,'1.1','all');
}
