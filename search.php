<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Food_Farm_Council
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) { ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'food-farm-council' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->
<section id="agencycontent">
<section id="agencybody" class="item">
		<div class="item agcontent agency-list">
			<?php
			
			$post_type = get_query_var("post_type");
			echo '<form class="search-form posts-search-form" role="search" action="/" autocomplete="off" method="get"><input class="agency-search-post-type" name="post_type" type="hidden" value="' . $post_type . '" />
				<input id="s" class="search-field form-control agency-search-s" autocomplete="off" name="s" type="search" value="' . get_search_query() . '" />
				<button class="search-submit btn" type="submit">Search</button></form>';
			if ($post_type == 'post') {
					echo '<div class="indexcontent">';
				}
			/* Start the Loop */
			while ( have_posts() ) {
				the_post();

				
				if ($post_type == 'post') {
					get_template_part( 'template-parts/content', 'index' );
				}
				else if ($post_type == 'agency') {
				
				$training_topic_array_class = "";
				$agency_category_terms = get_the_terms( $post->ID, 'agency-category' );
				$agency_category_array = array();
				foreach ($agency_category_terms as $t) {
				$id = 'agency-category-' . $t->term_id;
				array_push($agency_category_array, $id);
				}
				$training_topic_array_class = implode(' ', $agency_category_array);
					
				echo '<div class="agency-list-item ' . $training_topic_array_class . '">';
				the_title( '<h2 class="agency-title">', '</h2>' );
				echo '<div class="agency-overview">';
				the_field('overview_text');
				echo '<div class="agency-link"><a href="' . get_permalink() . '">Learn More</a></div>';

				echo '</div>';
				echo '</div>';

				}
				else {
					get_template_part( 'template-parts/content', 'search' );
				}
			}
			if ($post_type == 'post') {
					echo '</div>';
				}
			the_posts_navigation();

		} else {

			get_template_part( 'template-parts/content', 'none' );

		}
		?>
</div>
		</section>
</section>
	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();
