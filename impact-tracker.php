<section id="impacttracker">
<div class="impacttrackerinner">
	<div class="trackerbutton">
		<h3>Impact Tracker</h3>
		<div class="upcaret">
			<i class="fas fa-angle-up"></i>
		</div>
	</div>
	<?php

		// Check rows exists.
		if( have_rows('statistics', 'options') ):?>
		<ul>
		
		    <?php // Loop through rows.
		    while( have_rows('statistics', 'options') ) : the_row(); ?>
			<li>
		        <div class="item stattitle"><?php the_sub_field('title');?></div>
		        <div class="item statnumber"><?php the_sub_field('icon');?><?php the_sub_field('percentage');?></div>
			</li>
				<?php
		        // Do something...
		        
		
		    // End loop.
		    endwhile;?>
		</ul>
		
		<?php // No value.
		else :
		    // Do something...
		endif;
		?>
</div>
<script>
jQuery(document).ready(function( $ ) {
	$('.trackerbutton').on("click", function () {
		$('#impacttracker ul').slideToggle();
		$('.downcaret i').toggleClass("fa-angle-down");
	});
	$('#impacttracker ul').slideToggle();
	
});
</script>
</section>