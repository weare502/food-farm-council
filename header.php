<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Food_Farm_Council
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://kit.fontawesome.com/d5a7c51f9b.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.typekit.net/yri7wcu.css">
	<?php wp_head(); ?>
	
<script>
	
jQuery(document).ready(function( $ ) {
$('.menu-container .main-navigation .menu-item-has-children > a').after('<i class="fal fa-chevron-down" aria-hidden="true"></i>');
$(".menu-container .sub-menu").slideUp();

$('.menu-container .main-navigation .menu-item-has-children i').on("click touch", function () {
		var is = $('.menu-container .main-navigation .menu-item-has-children i').not($(this).parents("li").find("i"));
		$(is).next("ul").slideUp();
		$(this).next("ul").slideToggle();
		
		/*
		$(is).removeClass('fa-minus');
		$(this).toggleClass('fa-minus');
		*/
		$(is).removeClass('fa-chevron-up');
		$(is).addClass('fa-chevron-down');
		$(this).toggleClass('fa-chevron-up');
		$(this).toggleClass('fa-chevron-down');
		
		
	});
			
			
			$('#page .menu-toggle').on("click touch", function () {
			$('body').toggleClass('open');

			});	
				$('.menu-container .menu-toggle').on("click touch", function () {
			$('body').removeClass('open');

			});

});

function sidenav(){
	jQuery('body').toggleClass('open');
	jQuery('.menu-container .main-navigation .menu-item-has-children i').next("ul").slideUp();
	jQuery('.menu-container .main-navigation .menu-item-has-children i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
}
	</script>
	<?php the_field('header_scripts','options');?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="menu-container">
	<nav id="site-navigation" class="main-navigation">
		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="true"><span class="menucloseword">Close Menu</span><i class="fa fa-times" aria-hidden="true"></i></button>
		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		) );
		?>
	</nav>
</div>
<div class="side-nav-open-container" onClick="sidenav()"></div>
<div id="page" class="site">
	<div class="orangebox"></div>
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'food-farm-council' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php the_field('header_logo_svg','options');?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php the_field('header_logo_svg','options');?></a></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="menucloseword">Menu</span><i class="fa fa-bars" aria-hidden="true"></i></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
			<div class="searchicon" href="#agency-seach-popup">
				<i class="fas fa-search"></i>
				<script>
				jQuery(document).ready(function( $ ) {

					$('.searchicon').magnificPopup({
						type:'inline',
						midClick: true,
					});
				
				});
				</script>
			</div>
			<div id="agency-seach-popup" class="agency-seach-popup mfp-hide">
				<form class="search-form" role="search" action="/" autocomplete="off" method="get"><input class="agency-search-post-type" name="post_type" type="hidden" value="agency" />
				<input id="s" class="search-field form-control agency-search-s" autocomplete="off" name="s" type="search" value="" />
				<button class="search-submit btn" type="submit">Search</button></form></div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
