<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Food_Farm_Council
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="footercontainer">
			<div class="site-info">
				<div class="item footerlogo">
					<?php the_field('footer_logo_svg','options');?>
				</div>
				<div class="item addressinfo">
					<h5>Contact</h5>
					<?php

					// Check rows exists.
					if( have_rows('quick_links', 'options') ):?>
					<ul id="quicklinks">
					
					    <?php // Loop through rows.
					    while( have_rows('quick_links', 'options') ) : the_row(); ?>
						<li>
					        <a href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?></a>
						</li>
							<?php
					        // Do something...
					        
					
					    // End loop.
					    endwhile;?>
					</ul>
					
					<?php // No value.
					else :
					    // Do something...
					endif;
					?>
					<!--<?php the_field('footer_address','options');?>-->
					
					<?php

					// Check rows exists.
					if( have_rows('social_media', 'options') ):?>
					<ul id="footersocial">
					
					    <?php // Loop through rows.
					    while( have_rows('social_media', 'options') ) : the_row(); ?>
						<li>
					        <a href="<?php the_sub_field('link');?>"><?php the_sub_field('icon');?></a>
						</li>
							<?php
					        // Do something...
					        
					
					    // End loop.
					    endwhile;?>
					</ul>
					
					<?php // No value.
					else :
					    // Do something...
					endif;
					?>
				</div>
				<div class="item rightcontent">
					<p><?php the_field('footer_right_content','options');?></p>
				</div>									
			</div><!-- .site-info -->
			<div class="site-copy">
				<div class="item copyright">
					<p>Copyright <?php echo date("Y"); ?> <?php the_field('footer_copyright','options');?></p>
				</div>
				<div class="item privacypolicies">
					<p><a href="/privacy-policy/">Privacy Policies</a></p>
				</div>
				<div class="item designlogo">
					<a href="https://weare502.com/" target="_blank">Website Design by <?php get_template_part( "fragments/502logo");?></a>
				</div>								
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php the_field('footer_scripts','options');?>
<?php wp_footer(); ?>

</body>
</html>
