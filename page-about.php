<?php
/**
 * Template Name: About Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Food_Farm_Council
 */

get_header();
?>

	<main id="primary" class="site-main">
		
		<section id="introduction">
			<?php the_field('page_title');?>
		</section>
		
		<section id="introbox">
			<div class="introboxcontent"><?php the_field('intro_box');?></div>
			<div class="greenborder"></div>
		</section>
		
		<section id="introcols">	
			<?php

			// Check rows exists.
			if( have_rows('intro_columns') ):?>
			
			<ul>
			
			    <?php // Loop through rows.
			    while( have_rows('intro_columns') ) : the_row(); ?>
					<li>
			        <div class="item number"><?php the_sub_field('number');?></div>
			        <div class="item content"><?php the_sub_field('content');?></div>
					</li>
					<?php
			        // Do something...
			        
			
			    // End loop.
			    endwhile;
			?>
			</ul>
			<?php
			// No value.
			else :
			    // Do something...
			endif;
			?>
		</section>
		
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page-about' );

		endwhile; // End of the loop.
		?>


		<section id="councilgoals">
			<h2>Council Goals</h2>
			<?php

			// Check rows exists.
			if( have_rows('box_content') ):?>
			<ul>
			
				<?php // Loop through rows.
			    while( have_rows('box_content') ) : the_row(); ?>
				<li>
					<div class="greenbox">
				        <?php the_sub_field('content');?>
					</div>
			        <div class="orangeborder"></div>
				</li>
					<?php
			        // Do something...
			        
			
			    // End loop.
			    endwhile;?>
			</ul>
			
			<?php
			// No value.
			else :
			    // Do something...
			endif;
			?>
		</section>
		
		<section id="councilmembers">
			<h2>Meet the Council Members</h2>
			<?php

			// Check rows exists.
			if( have_rows('member') ):?>
			<ul>
			
				<?php // Loop through rows.
			    while( have_rows('member') ) : the_row(); ?>
				<li>
			        <div class="item image"><img src="<?php the_sub_field('image');?>"></div>
			        <div class="item name"><?php the_sub_field('name');?></div>
			        <div class="item title"><?php the_sub_field('title');?></div>
			        <div class="item content"><?php the_sub_field('content');?></div>
				</li>
					<?php
			        // Do something...
			        
			
			    // End loop.
			    endwhile;?>
			</ul>
			<?php
			
			// No value.
			else :
			    // Do something...
			endif;
			?>
		</section>
		
		<section id="councilrequirements">
			<h2>Council Membership Requirements</h2>
			<?php

			// Check rows exists.
			if( have_rows('requirement_columns') ):?>
			<ul>
			
			    <?php // Loop through rows.
			    while( have_rows('requirement_columns') ) : the_row(); ?>
				<li>
			        <?php the_sub_field('content');?>
				</li>
					<?php
			        // Do something...
			        
			
			    // End loop.
			    endwhile;?>
			</ul>
			
			<?php // No value.
			else :
			    // Do something...
			endif;
			?>
		</section>
		
		

		<!-- #main -->
	</main>

<?php
get_footer();
