<?php
$categories = get_categories( array(
    'orderby' => 'term_id',
    //'orderby' => 'name',
    //'order'   => 'ASC',
    'hide_empty' => false,
) );
//print_r($categories);
if ($categories) {
	echo '<ul id="educationbuttons">';
	foreach( $categories as $category ) {
		$name = $category->name;
		$catid = $category->term_id;
		$slug = $category->slug;
		$link = get_category_link( $catid );
		$feature_image = get_field( 'feature_image', 'category_' . $catid );
		echo '<li class="edubuttons">';
		echo '<a href="' . $link . '" data-categoryfilter-term="' . $slug . '">';
		echo '<img src="' . $feature_image . '">';
		echo '<div>' . $name . '</div>';
		echo '</a></li>';
	}
	/*
	echo '<li class="edubuttons">';
		echo '<a href="" data-categoryfilter-term="">';
		echo '<img><div>Show All</div>';
		echo '</a></li>';
	*/
	echo '</ul>';
	echo '<div class="showall"><a href="" data-categoryfilter-term="">Show All</a></div>';
	
?>
<script>
jQuery(document).ready(function( $ ) {
var categoryfilter = "";
var pagefilter = 1;
var stopload = false;
var canBeLoaded = true;
var num = 1;
function loadPosts() {
//alert("loadPosts");
	if (stopload == false && canBeLoaded == true ) {
	$.ajax({
				url: "/wp-admin/admin-ajax.php",
				data: "action=lookbookfilter&categoryfilter=" + categoryfilter + "&pagefilter=" + pagefilter,
				type: "POST",
				beforeSend:function(xhr){
					$(".loadingPosts").show();
					canBeLoaded = false;
				},
				success:function(data){
					if (data) {
					/*
						var htmlbefore = $('.indexcontent').html();
						//console.log(htmlbefore);
						//console.log(data);
						if (htmlbefore == data) {
							//console.log("same");
							//stopload = true;
						}
						else {
							//console.log("diff");
						}
						*/
						$('.indexcontent').html(data);
						/*
						var htmlafter = $('.indexcontent').html();
						if (htmlbefore == htmlafter) {
							//console.log("same");
							//stopload = true;
						}
						*/
					}
					
					$(".loadingPosts").hide();
					var pagesnum = $( ".indexcontent" ).attr("data-pagesnum");
					//console.log(pagesnum);
					if (pagesnum == "1") {
						//console.log("stop");
							stopload = true;
						}
					else {
						//alert("stop");
							stopload = false;
						}
					canBeLoaded = true;
				}
		});
		
	}
	//$(".loadingPosts").hide();
	
}

	$('#educationbuttons a, .showall a').on("click", function () {
		if ($( this ).hasClass( "activecat" )) {
			}
		else {
		//$('#educationbuttons a').not(this).removeClass("activecat");
		$('.activecat').not(this).removeClass("activecat");
		//$('#educationbuttons a').not(this).parent().removeClass("activecat");
		$('.activecat').not(this).parent().removeClass("activecat");
		//$( this ).toggleClass("activecat");
		//$( this ).parent().toggleClass("activecat");
		$( this ).addClass("activecat");
		$( this ).parent().addClass("activecat");
		var term = $( this ).data( "categoryfilter-term" );
		
		if ($( this ).hasClass( "activecat" )) {
			categoryfilter = term;
			//console.log("yes");
		}
		else {
			categoryfilter = "";
			//console.log("no");
		}
		pagefilter = 1;
		stopload = false;
		canBeLoaded = true;
		loadPosts();
		}
		return false;
	});
	
	
	$('.loadmore').on("click", function () {
		pagefilter++;
		loadPosts();
		return false;
	});
	
	
	$(window).scroll(function(){
	var el = $('.indexcontent');
	//var el = $('#colophon');
	//var bottomOffset = 500;
	var bottomOffset = 0;
	var scrollpoint = el.offset().top + el.outerHeight() - bottomOffset;
	//var scrollpoint = el.offset().top;
	//footerOffset = $('footer').offset().top;
	//bottomOffset = $(window).height() + footerOffset;
	//if( $(document).scrollTop() > (  scrollpoint )){
	if( $(document).scrollTop() + $(window).height() > (  scrollpoint )){
		pagefilter++;
		loadPosts();
		return false;
	}
	});
	
	loadPosts();
});
</script>
<!-- <a href="#" class="loadmore">Load More</a> -->
<?php
}

//global $wp_filter;
//print_r($wp_filter['pre_get_posts']);

?>