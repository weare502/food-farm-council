<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Food_Farm_Council
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );


		endwhile; // End of the loop.
		?>
		
		<section id="homebox">
			<?php

			// Check rows exists.
			if( have_rows('blocks') ):?>
			<ul id="blocks">
			
			    <?php // Loop through rows.
			    while( have_rows('blocks') ) : the_row(); ?>
				<li>
			        <a href="<?php the_sub_field('link');?>">
				        <img src="<?php the_sub_field('icon');?>">
						<?php the_sub_field('title');?>
				    </a>
				</li>
					<?php
			        // Do something...
			        
			
			    // End loop.
			    endwhile;?>
			</ul>
			
			<?php // No value.
			else :
			    // Do something...
			endif;
			?>
			
		</section>
		
		<section id="homefood">
			<img src="<?php the_field('vegetable_photo','options');?>" />
		</section>
	</main><!-- #main -->

<?php
get_template_part('impact-tracker');
get_footer();
