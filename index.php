<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Food_Farm_Council
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="indexheader">
			<?php
			if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
				<?php single_post_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
		</div>
		
		<?php get_template_part('education-buttons');
		endif;
		?>
		
		<!--<form class="search-form posts-search-form" role="search" action="/" autocomplete="off" method="get"><input class="agency-search-post-type" name="post_type" type="hidden" value="post" />
				<input id="s" class="search-field form-control agency-search-s" autocomplete="off" name="s" type="search" value="" />
				<button class="search-submit btn" type="submit">Search</button></form>
		-->
		<img class="loadingPosts" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" style="display: none; margin-bottom: 30px;"/>
		<div class="indexcontent">
			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'index' );

			endwhile;

			//the_posts_navigation();

			else :
	
				get_template_part( 'template-parts/content', 'none' );
	
			endif;
			?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
