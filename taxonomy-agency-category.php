<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Food_Farm_Council
 */

get_header();

$term_id = get_queried_object_id();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) { ?>

			<header class="page-header">
			<?php
			
			$category_heading = get_field( 'category_heading', 'term_' . $term_id );
			if ($category_heading) {
				echo '<h1>' . $category_heading . '</h1>';
			}
			else {
			?>
				<h1>We've found an organization the can help!</h1>
			<?php } ?>
				<!-- //the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );-->
				
			</header><!-- .page-header -->

<section id="agencycontent">
		<section id="sidebar" class="item">
		<div class="sidecontent">
				<h4 class="filter-text"><?php the_field('filter_text', 'term_' . $term_id); ?></h4>
				<?php
				
				//$term_children = get_term_children( $term_id, 'agency-category' );
				$term_children = get_terms( array( 
					'taxonomy' => 'agency-category',
					'hide_empty' => false,
					'parent' => $term_id,
					'orderby' => 'term_id',
				) );
				//print_r($term_children);
				if ($term_children){
				echo '<ul class="agency-category-list">';
				foreach( $term_children as $cat ) {
			
					$name = $cat->name;
					$slug = $cat->slug;
					$term_id = $cat->term_id;
					echo '<li><label><input type="checkbox" name="agency-category-list-item" value="' . $slug . '" id="' . $slug . '" class="agency-category-list-item" data-agency-category-id="'. $term_id .'" /><span class="agency-category-checkbox"><i class="fas fa-check"></i></span>' . $name . '</label></li>';
				}
				echo '</ul>';
				}
				?>
				<a href="/">Back to Home</a>
		</div>
		</section>
		<section id="agencybody" class="item">
		<div class="item agcontent agency-list">
		
			<?php
			/* Start the Loop */
			while ( have_posts() ) {
				the_post();
				
				$training_topic_array_class = "";
				$agency_category_terms = get_the_terms( $post->ID, 'agency-category' );
				$agency_category_array = array();
				foreach ($agency_category_terms as $t) {
				$id = 'agency-category-' . $t->term_id;
				array_push($agency_category_array, $id);
				}
				$training_topic_array_class = implode(' ', $agency_category_array);
					
				echo '<div class="agency-list-item ' . $training_topic_array_class . '">';
				the_title( '<h2 class="agency-title">', '</h2>' );
				echo '<div class="agency-overview">';
				the_field('overview_text');
				echo '<div class="agency-link"><a href="' . get_permalink() . '">Learn More</a></div>';
				/*
				$agency_category_terms = get_the_terms( $post->ID, 'agency-category' );
				//print_r($agency_category_terms);
				foreach( $agency_category_terms as $t ) {
			
					$name = $t->name;
					$slug = $t->slug;
					$term_id = $t->term_id;
					echo $term_id . " | " ;
				}
				*/
				echo '</div>';
				echo '</div>';
				
				//get_template_part( 'template-parts/content', get_post_type() );

			}
			
			?>
			<div class="agency-list-none" style="display: none">No agencies found that match your search</div>
			<script>
			$( document ).ready(function() {
				
			function updateFilter(noscroll = false) {
				$('.agency-list-none').hide();
		var agencies = $(".agency-list .agency-list-item");
	  
		var categories = $('.agency-category-list-item:checked');
		if (categories) {
		$(categories).each(function() {
			catid = $(this).data("agency-category-id");
			agencies = agencies.filter(".agency-category-" + catid);
		});
		
		}
		
		$( ".agency-list .agency-list-item").show();
		$( ".agency-list .agency-list-item").hide();
		agencies.show();
		
		if (agencies.length == 0) {
			$('.agency-list-none').show();
		}
		//window.scrollTo(0, $("#agencycontent").offset().top);
		
		if (noscroll == false) {
			$("html, body").animate({ scrollTop: $("#agencycontent").offset().top });
		}
	  }
	  
		$('.agency-category-list-item').on("change", function() {
			updateFilter();
		});
				updateFilter(true);
			});
			</script>
			
			<?php
			the_posts_navigation();

		} else {

			get_template_part( 'template-parts/content', 'none' );

		}
		?>
</div>
		</section>
</section>
	</main><!-- #main -->

<?php
get_footer();
